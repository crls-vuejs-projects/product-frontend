import axios from "axios";

const PRODUCT_API_URL = "http://localhost:8081/product";

class ProductService {
  getAllProducts() {
    return axios.get(`${PRODUCT_API_URL}`);
  }

  getProductById(id) {
    return axios.get(`${PRODUCT_API_URL}/${id}`);
  }

  deleteProductByID(id) {
    return axios.delete(`${PRODUCT_API_URL}/${id}`);
  }

  updateProduct(product) {
    return axios.put(`${PRODUCT_API_URL}`, product);
  }

  createProduct(product) {
    return axios.post(`${PRODUCT_API_URL}`, product);
  }
}

export default new ProductService();