import { createRouter, createWebHistory } from 'vue-router'
import ReadProducts from '../components/ReadProducts.vue'
import UpdateProduct from '../components/UpdateProduct.vue'
import CreateProduct from '../components/CreateProduct.vue'

const routes = [
  {
    path: '/',
    name: 'ReadProducts',
    component: ReadProducts
  },
  {
    path: '/:id',
    name: 'UpdateProduct',
    component: UpdateProduct
  },
  {
    path: '/create',
    name: 'CreateProduct',
    component: CreateProduct
  }
]

const router = createRouter({
  history: createWebHistory(process.env.BASE_URL),
  routes
})

export default router
